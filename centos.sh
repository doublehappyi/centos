#refer:https://www.digitalocean.com/community/tutorials/how-to-install-git-on-a-centos-6-4-vps
# compilation tools for CentOS
yum -y groupinstall "Development Tools"

#make tools and compilers needed to transform source code into binary executables
yum -y install zlib-devel perl-ExtUtils-MakeMaker asciidoc xmlto openssl-devel

#make git support https
yum -y install curl-devel

